package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"net"
	"os/exec"
	"regexp"
)

type AppCtx struct {
}

func (appCtx *AppCtx) TcpScan(ctx *fiber.Ctx) error {
	ip := ctx.Query("target")
	port := ctx.Query("port")
	command := exec.CommandContext(context.Background(), "nc", "-z", "-w", "2", ip, port)
	_, err := command.Output()
	if err != nil {
		return ctx.SendString("down")
	}
	return ctx.SendString("open")
}

var nmapStatusLinePat, _ = regexp.Compile("(\\d+)(/(udp|tcp))?\\s+(open\\|filtered|open|filtered|closed)")

func (appCtx *AppCtx) UdpScan(ctx *fiber.Ctx) error {
	ip := ctx.Query("target")
	port := ctx.Query("port")

	command := exec.CommandContext(
		context.Background(),
		"sudo", "nmap", "-sU", "-p", port, ip,
	)
	out, err := command.Output()

	if err != nil {
		fmt.Println(err)
		return ctx.SendString("down")
	}
	res := nmapStatusLinePat.FindSubmatch(out)
	n := len(res)
	if n != 5 {
		return ctx.SendString("down")
	}
	return ctx.Send(res[4])
}

func DoIpLookup(host string) ([]byte, error) {
	ips, err := net.LookupIP(host)
	if err != nil {
		return nil, err
	}

	ipStrs := make([]string, len(ips))
	for i := range ips {
		ipStrs[i] = ips[i].String()
	}

	resp, err := json.Marshal(ipStrs)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (appCtx *AppCtx) IpLookup(ctx *fiber.Ctx) error {
	host := ctx.Query("target")
	resp, err := DoIpLookup(host)
	if err != nil {
		fmt.Println(err)
		return ctx.SendString("[]")
	}
	return ctx.Send(resp)
}

func main() {
	appCtx := AppCtx{}

	app := fiber.New(fiber.Config{
		Prefork:       true,
		CaseSensitive: true,
		StrictRouting: true,
	})
	app.Get("/tcp", appCtx.TcpScan)
	app.Get("/udp", appCtx.UdpScan)
	app.Get("/ips", appCtx.IpLookup)
	app.Listen(":8081")
}
