#!/bin/bash
set -e

cat <<EOF > /etc/systemd/system/fcheck.service
[Unit]
Description=fcheck.service
After=network-online.target

[Service]
ExecStart=/root/fcheck

[Install]
WantedBy=multi-user.target
EOF

systemctl enable fcheck.service
systemctl restart fcheck.service
journalctl -u fcheck.service -f

